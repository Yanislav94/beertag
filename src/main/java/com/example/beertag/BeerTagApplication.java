package com.example.beertag;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Scope;

@SpringBootApplication
@Scope
public class BeerTagApplication {

	public static void main(String[] args) {
		SpringApplication.run(BeerTagApplication.class, args);
	}
}
