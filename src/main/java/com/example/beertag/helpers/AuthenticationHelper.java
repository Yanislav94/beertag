package com.example.beertag.helpers;

import com.example.beertag.exceptions.AuthorizationException;
import com.example.beertag.exceptions.EntityNotFoundException;
import com.example.beertag.models.User;
import com.example.beertag.services.contracts.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.stereotype.Component;

@Component
public class AuthenticationHelper {
    private static final String AUTHORIZATION_HEADER_NAME = "Authorization";
    private static final String INVALID_AUTHORIZATION_ERROR = "Invalid authentication.";
    private final UserService userService;

    @Autowired
    public AuthenticationHelper(UserService userService) {
        this.userService = userService;
    }

    public User tryGetUser(HttpHeaders headers) {
        if (!headers.containsKey(AUTHORIZATION_HEADER_NAME)) {
            throw new AuthorizationException(INVALID_AUTHORIZATION_ERROR);
        }
        String authorizationTokens = headers.getFirst(AUTHORIZATION_HEADER_NAME);
        try {
            String username = getUsername(authorizationTokens);
            String password = getPassword(authorizationTokens);

            User user = this.userService.get(username);

            if (!user.getPassword().equals(password)) {
                throw new AuthorizationException(INVALID_AUTHORIZATION_ERROR);
            }
            return user;
        } catch (EntityNotFoundException e) {
            throw new AuthorizationException(INVALID_AUTHORIZATION_ERROR);
        }
    }

    private String getUsername(String authorizationTokens) {
        int firstSpace = authorizationTokens.indexOf(" ");
        if (firstSpace == -1) {
            throw new AuthorizationException(INVALID_AUTHORIZATION_ERROR);
        }
        return authorizationTokens.substring(0, firstSpace);
    }

    private String getPassword(String authorizationTokens) {
        int firstSpace = authorizationTokens.indexOf(" ");
        if (firstSpace == -1) {
            throw new AuthorizationException(INVALID_AUTHORIZATION_ERROR);
        }
        return authorizationTokens.substring(firstSpace + 1);
    }
}
