package com.example.beertag.helpers;

import com.example.beertag.models.Beer;
import com.example.beertag.models.dtos.BeerDTO;
import com.example.beertag.models.dtos.BeerViewDto;
import com.example.beertag.services.contracts.BeerService;
import com.example.beertag.services.contracts.StyleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class BeerMapper {
    private final StyleService styleService;
    private final BeerService beerService;

    @Autowired
    public BeerMapper(StyleService styleService, BeerService beerService) {
        this.styleService = styleService;
        this.beerService = beerService;
    }

    public Beer fromDTO(BeerDTO dto) {
        Beer beer = new Beer();
        beer.setName(dto.getName());
        beer.setAbv(dto.getAbv());
        beer.setStyle(this.styleService.get(dto.getStyleId()));
        return beer;
    }

    public Beer fromDTO(int id, BeerDTO dto) {
        Beer beer = fromDTO(dto);
        beer.setId(id);
        Beer repositoryBeer = this.beerService.getById(id);
        beer.setCreatedBy(repositoryBeer.getCreatedBy());
        return beer;
    }

    public BeerViewDto toDTO(Beer beer) {
        BeerViewDto dto = new BeerViewDto();
        dto.setName(beer.getName());
        dto.setAbv(beer.getAbv());
        dto.setCreatorName(beer.getCreatedBy().getFirsName());
        dto.setStyleName(beer.getStyle().getName());
        return dto;
    }
}
