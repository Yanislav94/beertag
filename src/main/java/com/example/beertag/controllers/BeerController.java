package com.example.beertag.controllers;

import com.example.beertag.exceptions.AuthorizationException;
import com.example.beertag.exceptions.DuplicateEntityException;
import com.example.beertag.exceptions.EntityNotFoundException;
import com.example.beertag.helpers.AuthenticationHelper;
import com.example.beertag.helpers.BeerMapper;
import com.example.beertag.models.Beer;
import com.example.beertag.models.dtos.BeerDTO;
import com.example.beertag.models.User;
import com.example.beertag.models.dtos.BeerViewDto;
import com.example.beertag.services.contracts.BeerService;
import jakarta.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import java.util.List;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/api/beers")
public class BeerController {
    private final BeerService service;
    private final AuthenticationHelper authenticationHelper;
    private final BeerMapper beerMapper;

    @Autowired
    public BeerController(BeerService beerService, AuthenticationHelper authenticationHelper, BeerMapper beerMapper) {
        this.service = beerService;
        this.authenticationHelper = authenticationHelper;
        this.beerMapper = beerMapper;
    }

    @GetMapping
    public List<BeerViewDto> getAll(
            @RequestParam(required = false) String name,
            @RequestParam(required = false) Double minAbv,
            @RequestParam(required = false) Double maxAbv,
            @RequestParam(required = false) Integer styleId,
            @RequestParam(required = false) String sortBy,
            @RequestParam(required = false) String sortOrder
    ) {
        return this.service.getAll(name, minAbv, maxAbv, styleId, sortBy, sortOrder)
                .stream()
                .map(this.beerMapper::toDTO)
                .collect(Collectors.toList());
    }


    @GetMapping("/{id}")
    public BeerViewDto getById(@PathVariable int id) {
        try {
            return this.beerMapper.toDTO(this.service.getById(id));
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }
    }

    @GetMapping("/name")
    public Beer getByName(@RequestParam String name) {
        try {
            return this.service.getByName(name);
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        }
    }


    @PostMapping
    public Beer create(@Valid @RequestBody BeerDTO beerDTO, @RequestHeader HttpHeaders headers) {
        try {
            User loggedUser = this.authenticationHelper.tryGetUser(headers);
            Beer beer = this.beerMapper.fromDTO(beerDTO);
            beer.setCreatedBy(loggedUser);
            this.service.create(beer);
            return beer;
        } catch (DuplicateEntityException e) {
            throw new ResponseStatusException(HttpStatus.CONFLICT, e.getMessage());
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        } catch (AuthorizationException e) {
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, e.getMessage());
        }
    }


    @PutMapping("/{id}")
    public Beer update(@Valid @RequestBody BeerDTO beerDTO, @PathVariable int id, @RequestHeader HttpHeaders headers) {
        try {
            User loggedUser = this.authenticationHelper.tryGetUser(headers);
            Beer beer = this.beerMapper.fromDTO(id, beerDTO);
            this.service.update(beer, loggedUser);
            return beer;
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        } catch (AuthorizationException e) {
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, e.getMessage());
        } catch (DuplicateEntityException e) {
            throw new ResponseStatusException(HttpStatus.CONFLICT, e.getMessage());
        }
    }


    @DeleteMapping("/{id}")
    public void delete(@PathVariable int id, @RequestHeader HttpHeaders headers) {
        try {
            User loggedUser = this.authenticationHelper.tryGetUser(headers);
            this.service.delete(id, loggedUser);
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        } catch (AuthorizationException e) {
            throw new ResponseStatusException(HttpStatus.UNAUTHORIZED, e.getMessage());
        }
    }
}
