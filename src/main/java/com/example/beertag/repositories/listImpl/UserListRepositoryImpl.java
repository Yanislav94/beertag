package com.example.beertag.repositories.listImpl;

import com.example.beertag.exceptions.EntityNotFoundException;
import com.example.beertag.models.User;
import com.example.beertag.repositories.contracts.UserRepository;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

//@Repository
public class UserListRepositoryImpl implements UserRepository {
    private final List<User> users;

    public UserListRepositoryImpl() {
        users = new ArrayList<>();
        users.add(new User(1, "pesho", "pass1", true));
        users.add(new User(2, "tosho", "pass2", false));
        users.add(new User(3, "gosho", "pass3", false));
    }

    public List<User> getUsers() {
        return Collections.unmodifiableList(users);
    }

    @Override
    public List<User> getAll() {
        return this.users;
    }

//    @Override
    public void createUser(User user) {
        this.users.add(user);
    }

//    @Override
    public void updateUser(int id, User user) {
        User userToUpdate = getUserById(id);

        String newName = user.getUserName();
        String newEmail = user.getEmail();
        userToUpdate.setUserName(newName);
        userToUpdate.setEmail(newEmail);
    }

//    @Override
    public void deleteUser(User userToDelete) {
        this.users.remove(userToDelete);
    }

    @Override
    public User getUserById(int id) {
        return this.users
                .stream()
                .filter(b -> b.getId() == id)
                .findFirst()
                .orElseThrow(() -> new EntityNotFoundException("User", id));
    }

    @Override
    public User getUserByUserName(String userName) {
        return this.users
                .stream()
                .filter(b -> b.getUserName().equals(userName))
                .findFirst()
                .orElseThrow(() -> new EntityNotFoundException("User", "username", userName));
    }

    @Override
    public User getUserByUserEmail(String email) {
        return this.users
                .stream()
                .filter(b -> b.getEmail().equals(email))
                .findFirst()
                .orElseThrow(() -> new EntityNotFoundException("User", "email", email));
    }

    @Override
    public User get(String username) {
        return this.users.stream()
                .filter(u -> u.getUserName().equals(username))
                .findFirst()
                .orElseThrow(() -> new EntityNotFoundException("User", "Username", username));
    }
}
