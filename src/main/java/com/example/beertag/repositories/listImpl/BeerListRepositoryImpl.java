package com.example.beertag.repositories.listImpl;

import com.example.beertag.exceptions.EntityNotFoundException;
import com.example.beertag.models.Beer;
import com.example.beertag.repositories.contracts.BeerRepository;
import com.example.beertag.repositories.contracts.StyleRepository;
import com.example.beertag.repositories.contracts.UserRepository;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

//@Repository
public class BeerListRepositoryImpl implements BeerRepository {
    private final List<Beer> beers;
    private final StyleRepository styleRepository;

    public BeerListRepositoryImpl(UserRepository userRepository, StyleRepository styleRepository) {
        this.styleRepository = styleRepository;
        beers = new ArrayList<>();
        Beer beer = new Beer(1, "Glarus English Ale", 4.6);
        beer.setStyle(this.styleRepository.getById(1));
        beer.setCreatedBy(userRepository.getUserById(1));
        beers.add(beer);

        beer = new Beer(2, "Rhombus Porter", 5.0);
        beer.setStyle(this.styleRepository.getById(2));
        beer.setCreatedBy(userRepository.getUserById(1));
        beers.add(beer);

        beer = new Beer(3, "Opasen Char", 6.6);
        beer.setStyle(this.styleRepository.getById(3));
        beer.setCreatedBy(userRepository.getUserById(2));
        beers.add(beer);
    }

    @Override
    public List<Beer> getAll(String name, Double minAbv, Double maxAbv, Integer styleId, String sortBy, String sortOrder) {
        List<Beer> result = new ArrayList<>(this.beers);
        result = filterByName(result, name);
        result = filterByAbv(result, minAbv, maxAbv);
        result = filterByStyleId(result, styleId);
        sortResult(result, sortBy);
        orderResult(result, sortOrder);
        return result;
    }

    @Override
    public void create(Beer beer) {
        this.beers.add(beer);
    }

    @Override
    public void update(Beer beer) {
        Beer beerToUpdate = getBeerById(beer.getId());

        beerToUpdate.setName(beer.getName());
        beerToUpdate.setAbv(beer.getAbv());
        beerToUpdate.setStyle(beer.getStyle());
    }

    @Override
    public void delete(int id) {
        Beer beerToRemove = getBeerById(id);
        this.beers.remove(beerToRemove);
    }

    @Override
    public Beer getBeerById(int id) {
        return this.beers
                .stream()
                .filter(b -> b.getId() == id)
                .findFirst()
                .orElseThrow(() -> new EntityNotFoundException("Beer", id));
    }

    @Override
    public Beer getBeerByName(String name) {
        return this.beers
                .stream()
                .filter(b -> b.getName().equals(name))
                .findFirst()
                .orElseThrow(() -> new EntityNotFoundException("Beer", "name", name));
    }
    private void orderResult(List<Beer> beers, String sortOrder) {
        if (sortOrder != null && !sortOrder.trim().isEmpty()) {
            if (sortOrder.equalsIgnoreCase("desc")) {
                Collections.reverse(beers);
            }
        }
    }

    private void sortResult(List<Beer> beers, String sortBy) {
        if (sortBy != null && !sortBy.trim().isEmpty()) {
            switch (sortBy) {
                case "name" -> beers.sort((b1, b2) -> b1.getName().compareToIgnoreCase(b2.getName()));
                case "abv" -> beers.sort(Comparator.comparingDouble(Beer::getAbv));
                case "style" ->
                        beers.sort((b1, b2) -> b1.getStyle().getName().compareToIgnoreCase(b2.getStyle().getName()));
            }
        }
    }

    private List<Beer> filterByStyleId(List<Beer> beers, Integer styleId) {
        if (styleId != null) {
            beers = beers.stream().filter(b -> b.getStyle().getId() == styleId).collect(Collectors.toList());
        }
        return beers;
    }

    private List<Beer> filterByAbv(List<Beer> beers, Double minAbv, Double maxAbv) {
        return beers.stream().filter(b ->
                        (minAbv != null
                                ? (b.getAbv() > minAbv)
                                : true)
                                &&
                                (maxAbv != null
                                        ? b.getAbv() < maxAbv
                                        : true))
                .collect(Collectors.toList());
    }

    private List<Beer> filterByName(List<Beer> beers, String name) {
        if (name != null) {
            beers = beers.stream().filter(b -> b.getName().equals(name)).collect(Collectors.toList());
        }
        return beers;
    }
}
