package com.example.beertag.repositories.listImpl;

import com.example.beertag.exceptions.EntityNotFoundException;
import com.example.beertag.models.Style;
import com.example.beertag.repositories.contracts.StyleRepository;

import java.util.List;

//@Repository
public class StyleListRepositoryImpl implements StyleRepository {
    private final List<Style> styles;

    public StyleListRepositoryImpl(List<Style> styles) {
        this.styles = styles;
        styles.add(new Style(1, "Special Ale"));
        styles.add(new Style(2, "English Porter"));
        styles.add(new Style(3, "Indian Pale Ale"));
    }

    @Override
    public List<Style> getAll() {
        return this.styles;
    }

    @Override
    public Style getById(int id) {
        return this.styles.stream().filter(s -> s.getId() == id)
                .findFirst()
                .orElseThrow(() -> new EntityNotFoundException("Style", id));
    }
}
