package com.example.beertag.repositories;

import com.example.beertag.exceptions.EntityNotFoundException;
import com.example.beertag.models.User;
import com.example.beertag.repositories.contracts.UserRepository;
import org.springframework.context.annotation.PropertySource;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Repository;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

@Repository
@PropertySource("classpath:application.properties")
public class UserRepositoryImpl implements UserRepository {
    private static final String GET_ALL_USERS_QUERY = "SELECT user_id,username,password,is_admin FROM users";
    private static final String SELECT_USER_BY_ID_QUERY = "SELECT user_id,username,password,is_admin FROM users WHERE user_id = ?";
    private static final String INSERT_USER_QUERY = "INSERT INTO users (username, password, email, is_admin) ";
    private String dbUrl, username, password;

    public UserRepositoryImpl(Environment env) {
        this.dbUrl = env.getProperty("database.url");
        this.username = env.getProperty("database.username");
        this.password = env.getProperty("database.password");
    }

    @Override
    public List<User> getAll() {
//        try (Connection connection = DriverManager.getConnection(dbUrl, username, password);
//             Statement statement = connection.createStatement();
//             ResultSet resultSet = statement.executeQuery(GET_ALL_USERS_QUERY);
//        ) {
//            return resultSetToUserList(resultSet);
//        } catch (SQLException e) {
//            throw new RuntimeException(e);
//        }
        return null;
    }

//    @Override
//    public void createUser(User user) {
//        try (Connection connection = DriverManager.getConnection(dbUrl, username, password);
//             PreparedStatement statement = connection.prepareStatement(INSERT_USER_QUERY)) {
//            statement.setString(1, beer.getName());
//            statement.setDouble(2, beer.getAbv());
//            statement.setInt(3, beer.getStyle().getId());
//            statement.setInt(4, beer.getCreatedBy().getId());
//
//            statement.executeUpdate();
//
//        } catch (SQLException e) {
//            throw new RuntimeException(e);
//        }
//    }
//
//    @Override
//    public void updateUser(int id, User user) {
//
//    }
//
//    @Override
//    public void deleteUser(User userToDelete) {
//
//    }

    @Override
    public User getUserById(int id) {
//        try (Connection connection = DriverManager.getConnection(this.dbUrl, this.username, this.password);
//             PreparedStatement statement = connection.prepareStatement(SELECT_USER_BY_ID_QUERY);) {
//
//            statement.setInt(1, id);
//            try (ResultSet resultSet = statement.executeQuery()) {
//
//                List<User> users = resultSetToUserList(resultSet);
//                if (users.size() == 0) {
//                    throw new EntityNotFoundException("User", id);
//                }
//                return users.get(0);
//            }
//
//        } catch (SQLException e) {
//            throw new RuntimeException(e);
//        }
        return null;
    }

    @Override
    public User getUserByUserName(String userName) {
        return null;
    }

    @Override
    public User getUserByUserEmail(String email) {
        return null;
    }

    @Override
    public User get(String username) {
        return null;
    }

    private List<User> resultSetToUserList(ResultSet resultSet) throws SQLException {
//        List<User> result = new ArrayList<>();
//
//        while (resultSet.next()) {
//            int id = resultSet.getInt("user_id");
//            String username = resultSet.getString("username");
//            String password = resultSet.getString("password");
//            boolean isAdmin = resultSet.getBoolean("is_admin");
//
//            User user = new User(id, username, password, isAdmin);
//
//            result.add(user);
//        }
//        return result;
        return null;
    }
}
