package com.example.beertag.repositories.contracts;

import com.example.beertag.models.Beer;

import java.util.List;

public interface BeerRepository {
    List<Beer> getAll(String name, Double minAbv, Double maxAbv, Integer styleId, String sortBy, String sortOrder);

    void create(Beer beer);

    void update(Beer beer);

    void delete(int id);

    Beer getBeerById(int id);

    Beer getBeerByName(String name);
}
