package com.example.beertag.repositories.contracts;

import com.example.beertag.models.User;

import java.util.List;

public interface UserRepository {
    List<User> getAll();

//    void createUser(User user);

//    void updateUser(int id, User user);

//    void deleteUser(User userToDelete);

    User getUserById(int id);

    User getUserByUserName(String userName);
    User getUserByUserEmail(String email);

    User get(String username);
}
