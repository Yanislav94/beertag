package com.example.beertag.repositories;

import com.example.beertag.exceptions.EntityNotFoundException;
import com.example.beertag.models.Beer;
import com.example.beertag.repositories.contracts.BeerRepository;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.sql.SQLException;
import java.util.List;

@Repository
public class BeerRepositoryImpl implements BeerRepository {
    private final SessionFactory sessionFactory;

    @Autowired
    public BeerRepositoryImpl(SessionFactory sessionFactory) throws SQLException {
        this.sessionFactory = sessionFactory;
    }

    @Override
    public List<Beer> getAll(String name, Double minAbv, Double maxAbv, Integer styleId, String sortBy, String sortOrder) {
        try (Session session = sessionFactory.openSession()) {
            Query<Beer> fromBeer = session.createQuery("FROM Beer", Beer.class);
            return fromBeer.list();
        }
    }

    @Override
    public Beer getBeerById(int id) {
        try (Session session = sessionFactory.openSession()) {
            Beer beer = session.get(Beer.class, id);

            if (beer == null) {
                throw new EntityNotFoundException("Beer", id);
            }
            return beer;
        }
    }

    @Override
    public Beer getBeerByName(String name) {
        try (Session session = sessionFactory.openSession()) {
            Query<Beer> query = session.createQuery("FROM Beer b WHERE b.name = :name", Beer.class);
            query.setParameter("name", name);
            List<Beer> list = query.list();

            if (list.isEmpty()) {
                throw new EntityNotFoundException("Beer", "name", name);
            }
            return list.get(0);
        }
    }

    @Override
    public void create(Beer beer) {
        try (Session session = sessionFactory.openSession()) {
            session.save(beer);
        }
    }

    @Override
    public void update(Beer beer) {
        try (Session session = sessionFactory.openSession()) {
            session.beginTransaction();
            session.update(beer);
            session.getTransaction().commit();
        }
    }

    @Override
    public void delete(int id) {
        try (Session session = sessionFactory.openSession()) {
            session.beginTransaction();
            session.remove(getBeerById(id));
            session.getTransaction().commit();
        }
    }
}
