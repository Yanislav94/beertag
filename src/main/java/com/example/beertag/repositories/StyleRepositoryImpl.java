package com.example.beertag.repositories;

import com.example.beertag.exceptions.EntityNotFoundException;
import com.example.beertag.models.Style;
import com.example.beertag.repositories.contracts.StyleRepository;
import org.springframework.context.annotation.PropertySource;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Repository;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

@Repository
@PropertySource("classpath:application.properties")
public class StyleRepositoryImpl implements StyleRepository {
    private String dbUrl, username, password;
    private static final String SELECT_ALL_STYLES_QUERY = "SELECT * FROM styles";
    private static final String SELECT_STYLE_BY_ID_QUERY = "SELECT * FROM styles WHERE styles.style_id = ?";

    public StyleRepositoryImpl(Environment env) {
        this.dbUrl = env.getProperty("database.url");
        this.username = env.getProperty("database.username");
        this.password = env.getProperty("database.password");
    }

    @Override
    public List<Style> getAll() {
//        try (Connection connection = DriverManager.getConnection(dbUrl, username, password);
//             Statement statement = connection.createStatement();
//             ResultSet resultSet = statement.executeQuery(SELECT_ALL_STYLES_QUERY);
//        ) {
//            return resultSetToStyleList(resultSet);
//        } catch (SQLException e) {
//            throw new RuntimeException(e);
//        }
        return null;
    }

    @Override
    public Style getById(int id) {
//        try (Connection connection = DriverManager.getConnection(dbUrl, username, password);
//             PreparedStatement statement = connection.prepareStatement(SELECT_STYLE_BY_ID_QUERY)
//        ) {
//            statement.setInt(1, id);
//            try (ResultSet resultSet = statement.executeQuery()) {
//                if (!resultSet.next()) {
//                    throw new EntityNotFoundException("Style", id);
//                }
//
//                int styleId = resultSet.getInt("style_id");
//                String styleName = resultSet.getString("name");
//                return new Style(styleId, styleName);
//            }
//
//        } catch (SQLException e) {
//            throw new RuntimeException(e);
//        }
        return null;
    }

    private List<Style> resultSetToStyleList(ResultSet resultSet) throws SQLException {
//        List<Style> result = new ArrayList<>();
//
//        while (resultSet.next()) {
//            int id = resultSet.getInt("style_id");
//            String name = resultSet.getString("name");
//
//            Style style = new Style(id, name);
//
//            result.add(style);
//        }
//        return result;
        return null;
    }
}
