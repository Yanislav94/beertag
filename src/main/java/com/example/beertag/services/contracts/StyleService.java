package com.example.beertag.services.contracts;

import com.example.beertag.models.Style;

import java.util.List;

public interface StyleService {
    List<Style> getAll();

    Style get(int id);
}
