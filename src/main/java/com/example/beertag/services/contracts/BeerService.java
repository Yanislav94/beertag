package com.example.beertag.services.contracts;

import com.example.beertag.models.Beer;
import com.example.beertag.models.User;

import java.util.List;

public interface BeerService {
    List<Beer> getAll(String name, Double minAbv, Double maxAbv, Integer styleId, String sortBy, String sortOrder);

    Beer getById(int id);

    void create(Beer beer);

    void update(Beer beer, User loggedUser);

    void delete(int id, User loggedUser);

    Beer getByName(String name);
}
