package com.example.beertag.services.contracts;

import com.example.beertag.models.User;

import java.util.List;

public interface UserService {
    List<User> getAll();

    User getById(int id);

//    User createUser(User user);

//    User updateUser(int id, User user);

//    User deleteUser(int id);

    User get(String username);
}
