package com.example.beertag.services;

import com.example.beertag.exceptions.DuplicateEntityException;
import com.example.beertag.exceptions.EntityNotFoundException;
import com.example.beertag.models.User;
import com.example.beertag.repositories.contracts.UserRepository;
import com.example.beertag.services.contracts.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class UserServiceImpl implements UserService {

    private final UserRepository userRepository;

    @Autowired
    public UserServiceImpl(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    @Override
    public List<User> getAll() {
        return this.userRepository.getAll();
    }

    @Override
    public User getById(int id) {
        return this.userRepository.getUserById(id);
    }

//    @Override
//    public User createUser(User user) {
//        throwIfDuplicateUserExists(user);
//        this.userRepository.createUser(user);
//
//        return user;
//    }
//
//    @Override
//    public User updateUser(int id, User user) {
//        throwIfDuplicateUserExists(user);
//        this.userRepository.updateUser(id, user);
//
//        return user;
//    }
//
//    @Override
//    public User deleteUser(int id) {
//        User userToDelete = this.userRepository.getUserById(id);
//        this.userRepository.deleteUser(userToDelete);
//
//        return userToDelete;
//    }

    @Override
    public User get(String username) {
        return this.userRepository.get(username);
    }

    private void throwIfDuplicateUserExists(User user) {
        int userId = user.getId();
        String userName = user.getUserName();
        String userEmail = user.getEmail();

        boolean duplicateNameExists = nameExists(userName, userId);
        boolean duplicateEmailExists = emailExists(userEmail, userId);

        if (duplicateNameExists && duplicateEmailExists) {
            throw new DuplicateEntityException("User", "userName and email", String.format("'%s', '%s'", userName, userEmail));
        } else if (duplicateNameExists) {
            throw new DuplicateEntityException("User", "userName", userName);
        } else if (duplicateEmailExists) {
            throw new DuplicateEntityException("User", "email", userEmail);
        }
    }

    private boolean emailExists(String userEmail, int userId) {
        boolean duplicateEmailExists = true;
        try {
            User user = userRepository.getUserByUserEmail(userEmail);
            if (user.getId() == userId) {
                duplicateEmailExists = false;
            }
        } catch (EntityNotFoundException e) {
            duplicateEmailExists = false;
        }
        return duplicateEmailExists;
    }

    private boolean nameExists(String userName, int userId) {
        boolean duplicateNameExists = true;
        try {
            User user = userRepository.getUserByUserName(userName);
            if (user.getId() == userId) {
                duplicateNameExists = false;
            }
        } catch (EntityNotFoundException e) {
            duplicateNameExists = false;
        }
        return duplicateNameExists;
    }
}
