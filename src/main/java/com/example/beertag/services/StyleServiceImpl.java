package com.example.beertag.services;

import com.example.beertag.models.Style;
import com.example.beertag.repositories.contracts.StyleRepository;
import com.example.beertag.services.contracts.StyleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class StyleServiceImpl implements StyleService {
    private final StyleRepository styleRepository;

    @Autowired
    public StyleServiceImpl(StyleRepository styleRepository) {
        this.styleRepository = styleRepository;
    }

    @Override
    public List<Style> getAll() {
        return this.styleRepository.getAll();
    }

    @Override
    public Style get(int id) {
        return this.styleRepository.getById(id);
    }
}
