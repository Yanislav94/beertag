package com.example.beertag.services;

import com.example.beertag.exceptions.AuthorizationException;
import com.example.beertag.exceptions.DuplicateEntityException;
import com.example.beertag.exceptions.EntityNotFoundException;
import com.example.beertag.models.Beer;
import com.example.beertag.models.User;
import com.example.beertag.repositories.contracts.BeerRepository;
import com.example.beertag.services.contracts.BeerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class BeerServiceImpl implements BeerService {
    private static final String MODIFY_BEER_ERROR_MESSAGE = "Only admin or beer creator can modify a beer.";
    private final BeerRepository beerRepository;

    @Autowired
    public BeerServiceImpl(BeerRepository beerRepository) {
        this.beerRepository = beerRepository;
    }

    @Override
    public List<Beer> getAll(String name, Double minAbv, Double maxAbv, Integer styleId, String sortBy, String sortOrder) {
        return this.beerRepository.getAll(name, minAbv, maxAbv, styleId, sortBy, sortOrder);
    }

    @Override
    public Beer getById(int id) {
        return this.beerRepository.getBeerById(id);
    }

    @Override
    public Beer getByName(String name) {
        return this.beerRepository.getBeerByName(name);
    }

    @Override
    public void create(Beer beer) {
        boolean duplicateExists = true;

        String beerName = beer.getName();
        try {
            this.beerRepository.getBeerByName(beerName);
        } catch (EntityNotFoundException e) {
            duplicateExists = false;
        }

        if (duplicateExists) {
            throw new DuplicateEntityException("Beer", "name", beerName);
        }
        this.beerRepository.create(beer);
    }

    @Override
    public void update(Beer beer, User loggedUser) {
        boolean duplicateExists = true;
        checkPermissions(beer.getId(), loggedUser);
        String beerName = beer.getName();

        try {
            Beer repositoryBeer = this.beerRepository.getBeerByName(beerName);
            if (repositoryBeer.getId() == beer.getId()) {
                duplicateExists = false;
            }
        } catch (EntityNotFoundException e) {
            duplicateExists = false;
        }

        if (duplicateExists) {
            throw new DuplicateEntityException("Beer", "name", beerName);
        }

        this.beerRepository.update(beer);
    }


    @Override
    public void delete(int id, User loggedUser) {
        checkPermissions(id, loggedUser);
        this.beerRepository.delete(id);
    }


    private void checkPermissions(int beerId, User loggedUser) {
        Beer beer = this.beerRepository.getBeerById(beerId);
        if (!beer.getCreatedBy().equals(loggedUser) && !loggedUser.isAdmin()) {
            throw new AuthorizationException(MODIFY_BEER_ERROR_MESSAGE);
        }
    }
}
